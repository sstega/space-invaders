﻿using UnityEngine;

public abstract class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObject
{
    private static T instance;
    public static T I
    {
        get
        {
            if (instance != null)
            {
                return instance;
            }

            string filePath = $"Settings/{typeof(T).Name}";
            instance = Resources.Load<T>(filePath);
            return instance;
        }
    }
}