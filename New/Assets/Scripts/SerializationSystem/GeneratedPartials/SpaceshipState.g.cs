// --------------------------------------------------------
// Autogenerated for serialization purposes, do not edit
// --------------------------------------------------------


using SerializationSystem;

public partial class SpaceshipState
{
    public override void WriteBytes(System.IO.BinaryWriter writer)
    {
        Position.WriteBytes(writer);
        writer.Write(Health);
        writer.Write(Weapons.Count);
        foreach (var item in Weapons)
        {
            item.WriteBytes(writer);
        }
    }

    public override void ReadBytes(System.IO.BinaryReader reader)
    {
        Position.ReadBytes(reader);
        Health = reader.ReadInt32();
        Weapons = new ();
        int Weapons_itemCount = reader.ReadInt32();
        for (int i = 0; i < Weapons_itemCount; i++)
        {
            Weapons.Add(new());
            Weapons[i].ReadBytes(reader);
        }
    }
}