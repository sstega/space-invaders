using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Collections;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    private SpaceshipState state;
    private Vector2 previousPosition;
    private bool interp = false;

    private void Awake()
    {
        state = new SpaceshipState();
    }

    private void FixedUpdate()
    {
        previousPosition = state.Position;
        Vector2 deltaMove = new Vector2();
        deltaMove.x = InputManager.MoveHorizontal;
        deltaMove.y = InputManager.MoveVertical;
        deltaMove = deltaMove.normalized * (BattleSettings.I.SpaceshipSpeed * Time.deltaTime);
        
        state.Position += deltaMove;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            interp = !interp;
        }
        
        if (interp)
        {
            transform.position = LerpFixedTime(previousPosition, state.Position);
        }
        else
        {
            transform.position = state.Position;
        }
    }

    private Vector2 LerpFixedTime(in Vector2 a, in Vector2 b)
    {
        float t = (Time.time - Time.fixedTime) / Time.fixedDeltaTime;
        return Vector2.Lerp(a, b, t);
    }
}

public partial class SpaceshipState : BaseState
{
    public Vector2 Position;
    public int Health;
    public List<WeaponState> Weapons;
}

public partial class WeaponState : BaseState
{
    public string Name;
    public int Damage;
    public bool IsReady;
}

public abstract class BaseState
{
    public int ObjectId { get; private set; }

    public BaseState()
    {
        ObjectId = ObjectIdProvider.GetNext();
        Debug.Log($"Creating new state with ID {ObjectId}");
    }
    
    public virtual void WriteBytes(BinaryWriter writer) { }
    public virtual void ReadBytes(BinaryReader reader) { }
}

public static class ObjectIdProvider
{
    private static int currentId = 0;

    public static void Reset()
    {
        currentId = 0;
    }

    public static int GetNext()
    {
        int retVal = currentId;
        currentId++;
        return retVal;
    }
}