﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Vector2 lowerLeft, upperRight;
    public float horizontalMargin, bottomMargin, topMargin, spacing;
    [Range(0f, 1f)]
    public float stepAmount;
    [HideInInspector]
    public float stepLength;
    private Vector2 swarmLowerLeft, swarmUpperRight;

    public delegate void MoveTickDelegate(MoveDirection dir);
    public MoveTickDelegate OnMoveTick;

    public float tickInterval = 0.25f;
    public float spawnTickInterval = 0.1f;

    // INVADERS
    public List<Invader> invaderPrefabs;
    private float invaderWidth, invaderHeight;

    private static GameManager _instance;
    public static GameManager Instance 
    {
        get { return _instance; }
    }
    private void Awake()
    {
        _instance = this;

        // Find screen corners
        lowerLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        upperRight = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight));

        Debug.Log("Lower left: " + lowerLeft);
        Debug.Log("Upper right: " + upperRight);

        // Find swarm corners
        swarmLowerLeft = lowerLeft + new Vector2(horizontalMargin, bottomMargin); 
        swarmUpperRight = upperRight - new Vector2(horizontalMargin, topMargin);

        // Find width and height of largest invader
        invaderWidth = 0;
        invaderHeight = 0;
        float width, height;
        foreach (Invader invader in invaderPrefabs)
        {
            width = invader.GetComponent<SpriteRenderer>().sprite.bounds.size.x * invader.transform.localScale.x;
            if (width > invaderWidth) 
            {
                invaderWidth = width;
            }
            height = invader.GetComponent<SpriteRenderer>().sprite.bounds.size.y * invader.transform.localScale.y;
            if (height > invaderHeight) 
            {
                invaderHeight = height;
            }
        }
        stepLength = invaderWidth * stepAmount;

        // Start spawning!
        StartCoroutine(_spawnInvaders());
    }

    private IEnumerator _spawnInvaders()
    {
        float height = swarmUpperRight.y - swarmLowerLeft.y;
        float width = swarmUpperRight.x - swarmLowerLeft.x;
        int rows = Mathf.FloorToInt(height / (invaderHeight+spacing));
        int cols = Mathf.FloorToInt(width / invaderWidth);

        Vector2 upperLeft = new Vector2(swarmLowerLeft.x + invaderWidth/2, swarmUpperRight.y - invaderHeight/2);
        int invaderType = 0;
        int rowsRemaining = invaderPrefabs[invaderType].rows;
        for (int i=0; i < rows; i++)
        {
            if (rowsRemaining == 0)
            {
                invaderType++;
                if (invaderType >= invaderPrefabs.Count)
                {
                    break;
                }
                rowsRemaining = invaderPrefabs[invaderType].rows;
            }
            rowsRemaining--;

            for (int j=0; j < cols; j++)
            {
                Instantiate(invaderPrefabs[invaderType], upperLeft + new Vector2(j*invaderWidth, -i*(invaderHeight+spacing)), Quaternion.identity);
                yield return new WaitForSeconds(spawnTickInterval);
            }
        }

        // Start moving!
        StartCoroutine(_moveTick());
    }

    private IEnumerator _moveTick() 
    {
        int direction = 1;
        float freeSpace = (upperRight.x - lowerLeft.x) - (swarmUpperRight.x - swarmLowerLeft.x);
        int steps = Mathf.FloorToInt(freeSpace / stepLength);
        int stepsRemaining = steps / 2;
        while (true)
        {
            while (stepsRemaining > 0)
            {
                if (OnMoveTick != null)
                {
                    OnMoveTick((MoveDirection)direction);
                }
                yield return new WaitForSeconds(tickInterval);
                stepsRemaining--;
            }
            // Move down and reset steps
            stepsRemaining = steps;
            OnMoveTick(MoveDirection.DOWN);
            yield return new WaitForSeconds(tickInterval);
            direction *= -1;
        }
    }
}
